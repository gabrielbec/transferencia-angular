import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ComprovantePagamentoComponent } from './components/comprovante-pagamento/comprovante-pagamento.component';
import { ConfirmacaoTransferenciaComponent } from './components/confirmacao-transferencia/confirmacao-transferencia.component';
import { ConfirmarComponent } from './components/confirmar/confirmar.component';
import { ContatosPixComponent } from './components/contatos-pix/contatos-pix.component';
import { EnviarPixComponent } from './components/enviar-pix/enviar-pix.component';
import { HomeComponent } from './components/home/home.component';
import { MeusComprovantesComponent } from './components/meus-comprovantes/meus-comprovantes.component';
import { PaginaNaoEncontradaComponent } from './components/pagina-nao-encontrada/pagina-nao-encontrada.component';
import { PixInicialComponent } from './components/pix-inicial/pix-inicial.component';
import { QrcodeComponent } from './components/qrcode/qrcode.component';
import { TransferenciaTedDocComponent } from './components/transferencia-ted-doc/transferencia-ted-doc.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent, data: {title: false, depth: 1} },
  { path: 'comprovante-pagamento', component: ComprovantePagamentoComponent },
  { path: 'confirmacao-transferencia', component: ConfirmacaoTransferenciaComponent },
  { path: 'confirmar', component: ConfirmarComponent },
  { path: 'contatos-pix', component: ContatosPixComponent, data: {title: false, depth: 2}},
  { path: 'enviar-pix', component: EnviarPixComponent },
  { path: 'meus-comprovantes', component: MeusComprovantesComponent, data: {title: false, depth: 2}},
  { path: 'pix-inicial', component: PixInicialComponent, data: {title: false, depth: 2}},
  { path: 'qrcode', component: QrcodeComponent },
  { path: 'transferencia-ted-doc', component: TransferenciaTedDocComponent, data: {title: false, depth: 2}},
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '**', component: PaginaNaoEncontradaComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
