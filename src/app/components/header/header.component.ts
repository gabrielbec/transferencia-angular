import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  nomePg = window.location.pathname
  location?: any;
  
  constructor(private router: Router, private activetedRouter: ActivatedRoute) {
      //console.log(this.activetedRouter.snapshot.url)
      
   }

  ngOnInit(): void {
      //console.log(window.location.pathname)
      this.router.events.subscribe(
        (event: any) => {
          if (event instanceof NavigationEnd) {
            //console.warn('this.router.url', this.router.url);
            this.alternarNomePagina(this.router.url)
          }
        }
      );
      
  }

  alternarNomePagina(valor: any){
    switch (valor) {
      case '/':
      case '/home':
        this.nomePg = "Seja bem-vindo, Usuário!"
        break;
      case '/comprovante-pagamento':
        this.nomePg = "Comprovante de Transferência"
        break;
      case '/confirmacao-transferencia':
        this.nomePg = "Confirme os dados de transferência"
        break;
      case '/confirmar':
      case '/contatos-pix':
      case '/enviar-pix':
      case '/pix-inicial':
      case '/qrcode':
        this.nomePg = "pix"
        break;
      case '/meus-comprovantes':
        this.nomePg = "Meus Comprovantes"
        break;
      case '/transferencia-ted-doc':
        this.nomePg = "Preencha os dados da transferência"
        break;
      default:
        this.nomePg = "Pagina não encontrada"
        break;
    }
  }


}
