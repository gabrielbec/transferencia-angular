import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmarComponent } from './confirmar.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    ConfirmarComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    ConfirmarComponent
  ]
})
export class ConfirmarModule { }
