import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PixInicialComponent } from './pix-inicial.component';
import {MatCardModule} from '@angular/material/card';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import { AppRoutingModule } from 'src/app/app-routing.module';




@NgModule({
  declarations: [
    PixInicialComponent
  ],
  imports: [
    CommonModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    AppRoutingModule
  ],
  exports:[
    PixInicialComponent
  ]
})
export class PixInicialModule { }
