import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PixInicialComponent } from './pix-inicial.component';

describe('PixInicialComponent', () => {
  let component: PixInicialComponent;
  let fixture: ComponentFixture<PixInicialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PixInicialComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PixInicialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
