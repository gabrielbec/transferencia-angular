import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MeusComprovantesComponent } from './meus-comprovantes.component';

describe('MeusComprovantesComponent', () => {
  let component: MeusComprovantesComponent;
  let fixture: ComponentFixture<MeusComprovantesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MeusComprovantesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MeusComprovantesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
