import { Comprovante } from "./comprovante"

export let COMPROVANTES: Comprovante[] = [
    {
        data: new Date("07/09/2021"),
        operacao: 'PIX - Transferência',
        valor: 60,
        identificacao: 'PIX Debito Luz',
    },
    {
        data: new Date("07/10/2021"),
        operacao: 'TED - Agendamento',
        valor: 100,
        identificacao: 'Pagamento Aluguel',
    },
    {
        data: new Date("06/10/2021"),
        operacao: 'TED - Agendamento',
        valor: 150,
        identificacao: 'Pagamento Aluguel',
    },
    {
        data: new Date("03/09/2021"),
        operacao: 'PIX - Transferência',
        valor: 60,
        identificacao: 'PIX Debito Luz',
    },
    {
        data: new Date("03/10/2021"),
        operacao: 'TED - Agendamento',
        valor: 100,
        identificacao: 'Pagamento Aluguel',
    },
    {
        data: new Date("06/10/2021"),
        operacao: 'TED - Agendamento',
        valor: 155,
        identificacao: 'Pagamento Aluguel',
    },
    {
        data: new Date("11/10/2020"),
        operacao: 'TED - Agendamento',
        valor: 175.50,
        identificacao: 'Pagamento Aluguel',
    },
]