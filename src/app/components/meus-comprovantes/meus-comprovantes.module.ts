import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MeusComprovantesComponent } from './meus-comprovantes.component';
import { TabelaComprovantesComponent } from './tabela-comprovantes/tabela-comprovantes.component';
import { AppRoutingModule } from 'src/app/app-routing.module';



@NgModule({
  declarations: [
    MeusComprovantesComponent,
    TabelaComprovantesComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule
  ],
  exports: [
    MeusComprovantesComponent
  ]
})
export class MeusComprovantesModule { }
