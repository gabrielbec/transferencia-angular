import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { Comprovante } from './comprovante';
import { COMPROVANTES } from './mock-comprovantes';

@Injectable({
  providedIn: 'root'
})
export class ComprovanteService {

  constructor(private http: HttpClient) { }

  private readonly API = "http://localhost:3000/comprovantes"

  getComprovantesHttp(opcao: number): Observable<Comprovante[]>{
    switch (opcao) {
      case 2:
        return this.http.get<Comprovante[]>(this.API+"?operacao=TED - Agendamento")        
        break;
      case 3:
        return this.http.get<Comprovante[]>(this.API+"?operacao=DOC - Agendamento")        
        break;
      case 4:
        return this.http.get<Comprovante[]>(this.API+"?operacao=PIX - Transferência")        
        break;
      default:
        return this.http.get<Comprovante[]>(this.API)
        break;
    }
  }

  getComprovanteFiltroPeriodo(filtro: number, comprovantes: Comprovante[]): Comprovante[]{
    switch (filtro) {
      case 1:
        return comprovantes.filter(c => this.filtrarData(c) <= 1)
        break;
      case 2:
        return comprovantes.filter(c => this.filtrarData(c) <= 3)
        break;
      case 3:
        return comprovantes.filter(c => this.filtrarData(c) <= 6)
        break;
      case 4:
        return comprovantes.filter(c => this.filtrarData(c) <= 12)
        break;
      case 5:
        return comprovantes.filter(c => this.filtrarData(c) <= 24)
        break;
      default:
        return comprovantes
        break;
    }
  }

  filtrarData(comprovante: Comprovante): number{
    let hoje = new Date()
    let filtro = moment.duration(moment(hoje,"MM/DD/YYYY").diff(moment(comprovante.data,"MM/DD/YYYY"))).asMonths().toFixed();
    return parseInt(filtro)
  }
}
