export interface Comprovante {
    id?: number,
    data: Date,
    operacao: string,
    valor: number,
    identificacao: string
}
