import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { Comprovante } from './comprovante';
import { ComprovanteService } from './comprovante.service';

@Component({
  selector: 'app-meus-comprovantes',
  templateUrl: './meus-comprovantes.component.html',
  styleUrls: ['./meus-comprovantes.component.css']
})
export class MeusComprovantesComponent implements OnInit {

  comprovantes: Comprovante[] = []

  constructor(private comprovanteService: ComprovanteService) { }

  ngOnInit(): void {
    this.getComprovantes(0)
    
  }

  trocar(operacao: string, periodo: string){
    this.comprovanteService.getComprovantesHttp(parseInt(operacao)).subscribe(
      dados => {
        this.comprovantes = dados
        this.comprovantes = this.comprovanteService.getComprovanteFiltroPeriodo(parseInt(periodo), this.comprovantes)
        this.comprovantes.sort(this.ordenarPorData)
        console.log(this.comprovantes)
      }
    )
  }

  getComprovantes(op: number): void{
    this.comprovanteService.getComprovantesHttp(op).subscribe(dados => {
      this.comprovantes = dados
      this.comprovantes.sort(this.ordenarPorData)
    })
  }
  ordenarPorData(a?: any, b?: any){
    if (a.data > b.data) {
      return 1;
    }
    if (a.data < b.data) {
      return -1;
    }
    return 0;
  }
}
