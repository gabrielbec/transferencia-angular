import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TabelaComprovantesComponent } from './tabela-comprovantes.component';

describe('TabelaComprovantesComponent', () => {
  let component: TabelaComprovantesComponent;
  let fixture: ComponentFixture<TabelaComprovantesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TabelaComprovantesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TabelaComprovantesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
