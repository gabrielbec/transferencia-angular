import { Component, Input, OnInit } from '@angular/core';
import { Comprovante } from '../comprovante';
import { ComprovanteService } from '../comprovante.service';

@Component({
  selector: 'app-tabela-comprovantes',
  templateUrl: './tabela-comprovantes.component.html',
  styleUrls: ['./tabela-comprovantes.component.css']
})
export class TabelaComprovantesComponent implements OnInit {

  @Input() comprovantes: Comprovante[] = []

  constructor() { }

  ngOnInit(): void {
    
  }

  

}
