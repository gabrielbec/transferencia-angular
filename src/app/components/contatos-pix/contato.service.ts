import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Contato } from './contato';
import { CONTATOS } from './mock-contatos';

@Injectable({
  providedIn: 'root'
})
export class ContatoService {

  constructor(private http: HttpClient) { }

  private baseUrl = "http://localhost:3000/contatos"

  getContatosHttp(){
    return this.http.get<Contato[]>(this.baseUrl)
  }

  getContatoPorNomeHttp(nome: string){
    return this.http.get<Contato[]>(`${this.baseUrl}?nome=${nome.trim()}`)
  }
  
  remove(id: any){
    return this.http.delete(`${this.baseUrl}/${id}`)
  }

  salvarContato(contato: Contato): Observable<Contato>{
    return this.http.post<Contato>(this.baseUrl, contato)
  }

  atualizarContato(contato: Contato): Observable<Contato>{
     return this.http.put<Contato>(`${this.baseUrl}/${contato.id}`,contato)
  }

}
