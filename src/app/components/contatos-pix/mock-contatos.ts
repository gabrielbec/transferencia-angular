import { Contato } from "./contato";

export let CONTATOS: Contato[] = [
    {id: 1, nome: "João", numeroPix: "111.111.111.11"},
    {id: 2, nome: "Rogerio", numeroPix: "111.111.111.11"},
    {id: 3, nome: "Alice", numeroPix: "111.111.111.11"},
    {id: 4, nome: "Sandra", numeroPix: "111.111.111.11"}
]