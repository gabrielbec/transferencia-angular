import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContatosPixComponent } from './contatos-pix.component';

describe('ContatosPixComponent', () => {
  let component: ContatosPixComponent;
  let fixture: ComponentFixture<ContatosPixComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContatosPixComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContatosPixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
