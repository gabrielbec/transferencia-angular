import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Contato } from '../contato';
import { ContatoService } from '../contato.service';

@Component({
  selector: 'app-modal-generico',
  templateUrl: './modal-generico.component.html',
  styleUrls: ['./modal-generico.component.css']
})
export class ModalGenericoComponent implements OnInit {

  @Input() contato: Contato = {
    nome: '',
    numeroPix: ''
  }

  constructor(public activeModal: NgbActiveModal, private contatoService: ContatoService, private router: Router) { }

  ngOnInit(): void {
  }

  deletarContato(): void{
    //this.contatoService.deleteContato(this.contato)
    this.contatoService.remove(this.contato.id).subscribe(
      success => this.ngOnInit(),
      error => console.error("Error ao remover curso")
    )
    
    this.activeModal.close('Close click')
    
  }

}
