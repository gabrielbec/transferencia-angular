import { error } from '@angular/compiler/src/util';
import { Component, OnInit } from '@angular/core';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Contato } from './contato';
import { ContatoService } from './contato.service';

@Component({
  selector: 'app-contatos-pix',
  templateUrl: './contatos-pix.component.html',
  styleUrls: ['./contatos-pix.component.css']
})
export class ContatosPixComponent implements OnInit {

  contatos: Contato[] = []

  contato: Contato = {
    nome: "",
    numeroPix: ""
  }

  contatoPesquisa: Contato = {
    nome: "",
    numeroPix: ""
  }

  alertSuccess: string = 'd-none'
  alertSuccessEdit: string = 'd-none'
  alertDanger: string = 'd-none'
  alertWarning: string = 'd-none'

  pesquisa: boolean = false


  constructor(private contatoService: ContatoService, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.getContatos()
  }

  getContatos(): void {
    this.contatoService.getContatosHttp().subscribe(dados => {
      this.contatos = dados
      this.contatos.sort(this.ordenarPorNome)
    })
  }
  ordenarPorNome(a?: any, b?: any){
    if (a.nome > b.nome) {
      return 1;
    }
    if (a.nome < b.nome) {
      return -1;
    }
    return 0;
  }

  adicionarContato(nome: string, numeroPix: string): void {
    let contato: Contato = {
      nome,
      numeroPix
    }
    this.contatoService.salvarContato(contato).subscribe(c => this.contatos.push(c))
  }


  salvarContato(): void {
    let {nome, numeroPix} = this.contato // desestruturação
    if (nome.length == 0 || numeroPix.length == 0) {
      this.exibirAlerta(0)
    }else{
      this.contatoService.getContatoPorNomeHttp(nome).subscribe(
        dados => {
          if(dados.length == 1){
            this.exibirAlerta(1)
          }else{
            this.adicionarContato(nome, numeroPix)
            this.exibirAlerta(2)
            this.getContatos()
            this.limparInputs()
          }
        }
      )
    }

    /*
    else if(this.getContato(nome) != undefined){
      this.exibirAlerta(1)
    }else{
      this.adicionarContato(nome, numeroPix)
      this.exibirAlerta(2)
      this.getContatos()
      this.limparInputs()
    }
    /* */
  }

  exibirAlerta(tipoAlerta: number) {
    switch (tipoAlerta) {
      case 0:
        this.alertDanger = ''
        setTimeout(() => {
          this.alertDanger = 'd-none'
        }, 3500);
        break;
      case 1:
        this.alertWarning = ''
        setTimeout(() => {
          this.alertWarning= 'd-none'
        }, 3500);
        break;
      case 2:
        this.alertSuccess = ''
        setTimeout(() => {
          this.alertSuccess= 'd-none'
        }, 3500);
        break;
      case 3:
        this.alertSuccessEdit = ''
        setTimeout(() => {
          this.alertSuccessEdit = 'd-none'
        }, 3500);
        break;
      default:
        break;
    }

  }

  limparInputs(): void {
    this.contato.nome = ''
    this.contato.numeroPix = ''
  }

  pesquisarContato(nome: string): void{
    this.contatoService.getContatoPorNomeHttp(nome).subscribe(
      dados => {
        if(dados.length == 1){
          this.contatos = dados
        }else{
          this.getContatos()
        }
      }
    )
  }

  deletar(id: any){
    this.contatoService.remove(id).subscribe(
      success => this.getContatos(),
      error => console.error("Erro ao remover curso")
    )
    this.modalService.dismissAll() // Fecha o modal
  }

  atualizar: boolean = false

  atualizarContato(contato: Contato){
    this.contatoService.atualizarContato(contato).subscribe(
      success => {
        console.log("Atualizou")
        this.exibirAlerta(3)
      }
    )
  }
  abreModalContato(contato: Contato){
    this.atualizar = true
    this.contato = contato
  }
  
  closeResult = '';
  contatoDel: Contato = {
    nome: '',
    numeroPix: ''
  }

  open(content: any, contato: Contato) {
    this.contatoDel = contato
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

}
