import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContatosPixComponent } from './contatos-pix.component';
import { ItemContatoComponent } from './item-contato/item-contato.component';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalGenericoComponent } from './modal-generico/modal-generico.component';



@NgModule({
  declarations: [
    ContatosPixComponent,
    ItemContatoComponent,
    ModalGenericoComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    NgbModule
  ],
  exports: [
    ContatosPixComponent,
  ]
})
export class ContatosPixModule { }
