import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemContatoComponent } from './item-contato.component';

describe('ItemContatoComponent', () => {
  let component: ItemContatoComponent;
  let fixture: ComponentFixture<ItemContatoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemContatoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemContatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
