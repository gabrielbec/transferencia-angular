import { Component, Input, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Contato } from '../contato';
import { ContatoService } from '../contato.service';
import { ModalGenericoComponent } from '../modal-generico/modal-generico.component';

@Component({
  selector: 'app-item-contato',
  templateUrl: './item-contato.component.html',
  styleUrls: ['./item-contato.component.css']
})
export class ItemContatoComponent implements OnInit {

  @Input() contato: Contato = {
    nome: '',
    numeroPix: ''
  }

  constructor(private modalService: NgbModal) { }

  ngOnInit(): void {
  }

  open(){
    const modalRef = this.modalService.open(ModalGenericoComponent);
    modalRef.componentInstance.contato = this.contato;
  }

}
