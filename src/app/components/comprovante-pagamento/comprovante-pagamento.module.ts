import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComprovantePagamentoComponent } from './comprovante-pagamento.component';



@NgModule({
  declarations: [
    ComprovantePagamentoComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ComprovantePagamentoComponent
  ]
})
export class ComprovantePagamentoModule { }
