import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComprovantePagamentoComponent } from './comprovante-pagamento.component';

describe('ComprovantePagamentoComponent', () => {
  let component: ComprovantePagamentoComponent;
  let fixture: ComponentFixture<ComprovantePagamentoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComprovantePagamentoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComprovantePagamentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
