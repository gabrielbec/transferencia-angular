import { TestBed } from '@angular/core/testing';

import { TransferenciaTedDocService } from './transferencia-ted-doc.service';

describe('TransferenciaTedDocService', () => {
  let service: TransferenciaTedDocService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TransferenciaTedDocService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
