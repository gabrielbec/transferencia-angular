import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Transferencia } from './transferencia';
import { TransferenciaTedDocComponent } from './transferencia-ted-doc.component';



@Injectable({
  providedIn: 'root'
})
export class TransferenciaTedDocService {

  constructor(private http:HttpClient) { }



   // chamada das rotas HTTP
   private baseUrl = "http://localhost:3000/Transferencia"
  
  getTransferenciaHttp(){
    return this.http.get<Transferencia[]>(this.baseUrl)
  }

  postTransferenciaHttp(transferencia: Transferencia){
    return this.http.post<Transferencia>(this.baseUrl, transferencia)
  }
}
