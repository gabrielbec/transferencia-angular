import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransferenciaTedDocComponent } from './transferencia-ted-doc.component';

describe('TransferenciaTedDocComponent', () => {
  let component: TransferenciaTedDocComponent;
  let fixture: ComponentFixture<TransferenciaTedDocComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransferenciaTedDocComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferenciaTedDocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
