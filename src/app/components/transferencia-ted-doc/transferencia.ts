import { StringMap } from "@angular/compiler/src/compiler_facade_interface";

export interface Transferencia {
    id?: number,
    pix?: string,
    tipoTransferencia?: string,
    banco?: string,
    agencia?: string,
    conta?: string,
    digito?: string,
    cpf?: string,
    valor?: string,
}