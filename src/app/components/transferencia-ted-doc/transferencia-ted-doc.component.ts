import { Component, OnInit } from '@angular/core';
import { Transferencia } from './transferencia';
import { TransferenciaTedDocService } from './transferencia-ted-doc.service';
import { CommonModule } from '@angular/common';
import { FormControl, FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-transferencia-ted-doc',
  templateUrl: './transferencia-ted-doc.component.html',
  styleUrls: ['./transferencia-ted-doc.component.css']
})

export class TransferenciaTedDocComponent implements OnInit {
  
  selection : string = '';

  constructor(private transferenciateddocService: TransferenciaTedDocService) {}

  //Array com os valores do select
  bancos: string[] = ['Proway', '001 BANCO DO BRASIL S.A (BB)','237 BRADESCO S.A', '335 Banco Digio S.A','260 NU PAGAMENTOS S.A (NUBANK)','290 Pagseguro Internet S.A (PagBank)','380 PicPay Servicos S.A.','104 CAIXA ECONÔMICA FEDERAL (CEF)','033 BANCO SANTANDER BRASIL S.A','341 ITAÚ UNIBANCO S.A' ];
  tiposTransferencias: string [] = ['TED', 'DOC', 'PIX'];

 formularioTransferencia = new FormGroup({
  banco: new FormControl('', Validators.required),
  agencia: new FormControl('', Validators.required),
  conta: new FormControl('', Validators.required),
  digito: new FormControl('', Validators.required),
  cpf: new FormControl('', Validators.required),
  valor: new FormControl('', Validators.required),   
});

formPix =  new FormGroup({
  pix: new FormControl('', Validators.required),
})

//Capturando valores dos campos através formgrup
  onSubmit(){
    let transferencia: Transferencia = {
      
      tipoTransferencia: this.formularioTransferencia.value.tipoTransferencia,
      banco: this.formularioTransferencia.value.banco,
      agencia: this.formularioTransferencia.value.agencia,  
      conta: this.formularioTransferencia.value.conta,
      digito: this.formularioTransferencia.value.digito,
      cpf: this.formularioTransferencia.value.cpf,
      valor: this.formularioTransferencia.value.valor,
    }
    let transferenciaPix: Transferencia = {
      pix: this.formPix.value.pix,
    }
    if (this.selection != "PIX"){
      this.adicionarTransferencia(transferencia)
      
    }else{
      this.adicionarTransferencia(transferenciaPix)
    }
  }

  transferencias: Transferencia[] = []  

  ngOnInit(): void {}    
  
  getTransferencias(): void {
    this.transferenciateddocService.getTransferenciaHttp().subscribe(dados =>{
      this.transferencias = [...dados] 
    })
  }

  adicionarTransferencia(transferencia: Transferencia): void {
      this.transferenciateddocService.postTransferenciaHttp(transferencia).subscribe(resultado => console.log(resultado))
  }

  Choose(selection : string){
    this.selection = selection;
  }
}