import { TransferenciaTedDocComponent } from './components/transferencia-ted-doc/transferencia-ted-doc.component';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { EnviarPixComponent } from './components/enviar-pix/enviar-pix.component';
import { ContatosPixModule } from './components/contatos-pix/contatos-pix.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PixInicialModule } from './components/pix-inicial/pix-inicial.module';
import { MeusComprovantesModule } from './components/meus-comprovantes/meus-comprovantes.module';
import { PaginaNaoEncontradaComponent } from './components/pagina-nao-encontrada/pagina-nao-encontrada.component';
import { ConfirmarModule } from './components/confirmar/confirmar.module';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    EnviarPixComponent,
    TransferenciaTedDocComponent,
    PaginaNaoEncontradaComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ContatosPixModule,
    BrowserAnimationsModule,
    PixInicialModule,
    MeusComprovantesModule,
    ConfirmarModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
